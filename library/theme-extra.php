<?php 

/**
* Add excerpt support to pages
*
*/


function blogcommuns_add_excerpts_to_pages() {
     add_post_type_support( 'page', 'excerpt' );
}

add_action( 'init', 'blogcommuns_add_excerpts_to_pages' );

/**
* Get a page by its slug and return either the excerpt or the content, with or without the title
* Return an HTML string
*/

if ( ! function_exists( 'blogcommuns_page_as_widget' ) ) :
	function blogcommuns_page_as_widget( $page_slug , $full_content = false, $with_title = false, $heading_size='h4' ) {
		
		
		$page = get_page_by_path($page_slug);
		return "<p>".apply_filters('the_content',$page->post_content)."</p>";

	}
endif;