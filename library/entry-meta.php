<?php
/**
 * Entry meta information for posts
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

if ( ! function_exists( 'foundationpress_entry_meta' ) ) :
	function foundationpress_entry_meta() {
		echo '<p class="byline author">'.get_the_author() . sprintf( __( ', %1$s.', 'foundationpress' ), get_the_date() ) . '</p>';
		
	}
endif;
