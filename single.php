<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<div id="single-post" role="main">
<?php get_sidebar(); ?>

<?php do_action( 'foundationpress_before_content' ); ?>
<?php while ( have_posts() ) : the_post(); ?>
	<div id="sidebar-top-position">
		<article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">
			<div class="small-12 row column"><div class="separator"></div>
			<header>
				<h1 class="entry-title"><?php the_title(); ?></h1>
				<?php $type = get_field('type_darticle'); ?>

			<?php if ($type=="Curation") : ?>
				<div class="row">
					<div class="small-5 column">
						<p class="byline source">
							<?php if(get_field('type_de_source')) : echo "[".get_field('type_de_source')."]"; endif; ?>
						</p>
					</div>
					<div class="small-4 column">
						<p class="byline author">
							<?php if(get_field('auteur')) : echo get_field('auteur');  endif; ?>
							<?php if(get_field('auteur')) : echo get_field('date');  endif; ?>
						</p>

					</div>
				</div>
			<?php else : ?>
				<p class="byline author">
					<?php foundationpress_entry_meta(); ?>				
				</p>
			<?php endif; ?>
			


			</header>
			<?php do_action( 'foundationpress_post_before_entry_content' ); ?>
			<div class="entry-content">

			<?php if ( has_post_thumbnail() ) : ?>
					<div class="thumbnail-container" >
						<div class="thumbnail-bkg" style="background-image:url('<?php echo the_post_thumbnail_url();?>');"></div>
						<div class="thumbnail-img">
							<?php the_post_thumbnail();?>	
						</div>
						
					</div>
					<div class="thumbnail-legend">
							<?php echo get_post(get_post_thumbnail_id())->post_excerpt; ?>
						</div>
			<?php  endif; ?>

			<?php the_content(); ?>

			

			<?php if ($type=="Curation") : ?>
				<p class="byline  curation">
						<span class="curation-source-type"><?php if(get_field('type_de_source')) : echo "[".get_field('type_de_source')."]"; endif; ?></span><br>
						<?php if(get_field('titre')) : ?>
						<span class="curation-source-title">
							<?php if(get_field('lien_url')) : ?>
								<a href="<?php the_field('lien_url'); ?>">
							<?php endif; ?>
									<?php  echo get_field('titre'); ?>
									<?php if(get_field('auteur')) : echo ", "; echo get_field('auteur');  endif; ?>
									<?php if(get_field('editeur')) : echo ", "; echo get_field('editeur'); endif; ?>
									<?php if(get_field('date')) : echo ", "; echo get_field('date'); endif; ?>
									<?php if(get_field('licence')) : echo ", "; echo get_field('licence'); endif; ?>]
							<?php if(get_field('lien_url')) : ?>
								</a>
							<?php endif; ?>
							
						<?php endif; ?>

						
						
						
					<br>
					
				</p>
		
			<?php endif; ?>
			

			<?php edit_post_link( __( 'Edit', 'foundationpress' ), '<span class="edit-link">', '</span>' ); ?>
			</div>



			<footer>
				<?php wp_link_pages( array('before' => '<nav id="page-nav"><p>' . __( 'Pages:', 'foundationpress' ), 'after' => '</p></nav>' ) ); ?>
				<?php $tags = get_the_tags(); ?>
		<?php if ( $tags ) : ?> 
			<ul class="menu simple">
			<?php foreach ($tags as $tag) {
				echo '<li class="keyword"><a href="'.get_tag_link($tag->term_id).'">#'.$tag->name.'</a></li>';
			}
			?>
			</ul>
		<?php endif; ?>
			</footer>
			<?php //the_post_navigation(); ?>
			<?php do_action( 'foundationpress_post_before_comments' ); ?>
			<?php comments_template(); ?>
			<?php do_action( 'foundationpress_post_after_comments' ); ?>
			</div>
		</article>
	</div>
<?php endwhile;?>

<?php do_action( 'foundationpress_after_content' ); ?>
</div>
<?php get_footer();
