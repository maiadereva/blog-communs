<?php
/**
 * The sidebar containing the main widget area
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>
<aside class="sidebar" data-sticky-container >
	<div class="sidebar-content sticky" data-sticky data-top-anchor='sidebar-top-position:top'>
		<?php do_action( 'foundationpress_before_sidebar' ); ?>
		
		<div class="sidebar-logo">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
				<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/logo_lcda.png" alt="Les communs d'abord">
			</a>
		</div>

		<?php blogcommuns_main_nav(); ?>
		<?php blogcommuns_category_nav(); ?>
		<?php blogcommuns_page_nav(); ?>
		
		<div class="sidebar-about">
			<?php echo blogcommuns_page_as_widget('about',false,true,'h5'); ?>	
		</div>
		
		<?php dynamic_sidebar( 'sidebar-widgets' ); ?>
		<?php do_action( 'foundationpress_after_sidebar' ); ?>
	</div>
</aside>
