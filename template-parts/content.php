<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

<div id="post-<?php the_ID(); ?>" <?php post_class('blogpost-entry'); ?>>
	<a href="<?php the_permalink(); ?>" class="full-post-grid-link"><span></span></a>
	<script>
	jQuery('document').ready(function(){
		$('.full-post-grid-link').on('mouseenter',function(e){
			console.log('enter');
			$(this).parent().addClass('active-post');
		})
		$('.full-post-grid-link').on('mouseleave',function(e){
			console.log('leave');
			$(this).parent().removeClass('active-post');
		})		
	});
	</script>
	<header>
		
		<?php if ( has_post_thumbnail() ) : ?>
			<div class="thumbnail-container"><?php the_post_thumbnail();?></div>
		<?php endif; ?>
		<div class="categories"><?php the_category( ', ' ); ?></div>
		<h2 class="entry-title"><?php the_title(); ?></h2>
		
		<?php $type = get_field('type_darticle'); ?>
		<?php if ($type=="Curation") : ?>
				<p class="byline author">
						<?php if(get_field('auteur')) : echo stripslashes(get_field('auteur'));  endif; ?>
						<?php 
							if(get_field('date')) :
								$full_date = get_field('date');
								$y = substr($full_date,0,4);
								$m = substr($full_date,4,2);
								$d = substr($full_date,6,2);
						?>
								<?php  echo ", le "; echo $d."/".$m."/".$y;  ?>

						<?php endif; ?>
						<br>
						[<?php if(get_field('type_de_source')) : echo get_field('type_de_source'); endif; ?>]
					<br>

				</p>
		<?php else : ?>
				<?php foundationpress_entry_meta(); ?>				
		<?php endif; ?>
	</header>
	<div class="entry-content">
		<?php the_excerpt( __( 'Continue reading...', 'foundationpress' ) ); ?>
	</div>
	<footer>
		<?php $tags = get_the_tags(); ?>
		<?php if ( $tags ) : ?> 
			<ul class="menu simple">
			<?php foreach ($tags as $tag) {
				echo '<li class="keyword"><a href="'.get_tag_link($tag->term_id).'">#'.$tag->name.'</a></li>';
			}
			?>
			</ul>
		<?php endif; ?>

	</footer>
</div>