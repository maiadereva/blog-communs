<?php
/*
Template Name: Site Archives
*/
get_header(); ?>
<?php get_template_part( 'template-parts/featured-image' ); ?>

 <div id="page" role="main">

 <?php get_sidebar(); ?>

			
    		<article <?php post_class('main-content') ?> id="post-<?php the_ID(); ?>">

    			<div class="small-12 row column"><div class="separator"></div> 	


				<?php the_post(); ?>
				<h1 class="entry-title"><?php the_title(); ?></h1>
				
				
				<h2>Archives par mois :</h2>
				<ul>
					<?php wp_get_archives('type=monthly'); ?>
				</ul>
				
				<h2>Archives par sujets :</h2>
				<ul>
					 <?php wp_list_categories(); ?>
				</ul>

			</article>

</div><!-- #container -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>